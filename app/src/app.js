var app = angular.module('app', ['ngMaterial', 'firebase', 'ngRoute']);

app.constant('firebaseEndpoint', 'https://er-du-der-ikke.firebaseio.com/');

app.config(['$routeProvider',
  function($routeProvider) {
    $routeProvider.
      when('/settings', {
        templateUrl: 'views/settings.html',
        controller: 'SettingsController'
      }).
      when('/grid', {
        templateUrl: 'views/persongrid.html',
        controller: 'PersonGridController'
      })
      .otherwise({
        redirectTo: '/grid'
      });
  }]);

app.run(function ($rootScope, $location, AuthenticationService) {

  var routesThatDontRequireAuth = ['/', '/grid'];

  function requiresAuthentication(url) {
    return routesThatDontRequireAuth.indexOf(url) < 0;
  }

  $rootScope.$on('$routeChangeStart', function (event, next, current) {
    console.log('ulr, isLoggedIn', $location.url(), AuthenticationService.isLoggedIn());
    if (requiresAuthentication($location.url()) && !AuthenticationService.isLoggedIn()) {
      $location.path('/');
    }
  });

});