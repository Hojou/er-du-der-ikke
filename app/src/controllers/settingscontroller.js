app.controller('SettingsController', function ($scope, DataService, AuthenticationService, $filter) {

  $scope.employees = DataService.getEmployees();

  var auth = AuthenticationService.getAuth();

  function getAssigned(employees, id) {
    var match = $filter('filter')(employees, {assigned : id});
    return (match) ? match[0] : null;
  }

  $scope.employees.$loaded().then(function (data) {
    $scope.model = {
      displayName: auth.google.displayName,
      uid: auth.uid,
      assigned: getAssigned(data, auth.uid)
    };
  });


  $scope.assignEmployee = function (employee) {
    $scope.model.assigned = employee;
    DataService.assignEmployee(employee, auth.uid);
  }

  $scope.saveState = function () {
    DataService.updateEmployee($scope.model.assigned);
  }

});