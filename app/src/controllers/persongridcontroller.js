app.controller('PersonGridController', function ($scope, DataService, $filter, FirebaseService, $mdDialog) {

  $scope.dates = DataService.getDates();
  $scope.employees = DataService.getEmployees();
  var helpUserCreation = $mdDialog.alert({
      title: 'Vejledning',
      content: "Skriv navnet på personen i tekstfeltet. Tryk derefter enten på 'Enter' elller tryk på Opret knappen ud for brugernavn feltet.",
      ok: 'Luk'
  });
  
  var helpDateCreation = $mdDialog.alert({
      title: 'Vejledning',
      content: "Sæt fokus i dato feltet. Skriv derefter dato i det viste format eller benyt browserens kalender funktion til at vælge en specifik dato. Tryk derefter på Opret knappen ud for dato feltet",
      ok: 'Luk'
  });
  
  $scope.addEmployee = function(person) {
    console.log('person', person);
    DataService.createEmployee(person);
    person.name = '';
  };

  $scope.showHelp = function (windowName) {
    var windowReference;
    windowReference = (windowName === "user") ? helpUserCreation : helpDateCreation;
    $mdDialog.show(windowReference);
  }

  $scope.changeEmployeeName = DataService.updateEmployee;
  $scope.removeEmployee = DataService.removeEmployee;
  $scope.addDate = DataService.createDate;
  $scope.removeDate = DataService.removeDate;
  $scope.saveState = function (employee, date, state) {
    //if ($scope.auth.uid != employee.assigned)
    DataService.saveState(employee, date, state);
  };
  $scope.getState = DataService.getState;

});

