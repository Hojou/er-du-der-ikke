app.controller('AuthenticationController', function ($scope, $location, AuthenticationService) {


  function checkAuth(auth) {
    var authData = auth || AuthenticationService.getAuth();
    console.log('auth', authData);
    if (!authData) {
      delete $scope.auth;
      return;
    }

    switch (authData.provider) {
      case "google":
        var profile = authData.google.cachedUserProfile;
        $scope.auth = {
          displayName : profile.given_name,
          link: profile.link,
          picture: profile.picture,
          uid: authData.uid
        };    
        break;
      case "facebook":
        var profile = authData.facebook.cachedUserProfile;
        $scope.auth = {
          displayName : profile.first_name,
          link: profile.link,
          picture: profile.picture.data.url,
          uid: authData.uid
        };    
        break;
    }

  }

  $scope.logIn = function (provider) {
    AuthenticationService.authorize(provider).then(function(authData) {
      console.log("Logged in as:", authData.uid, authData);
      checkAuth(authData);
    }).catch(function(error) {
      console.error("Authentication failed:", error);
    });
  };

  $scope.logOut = function () {
    AuthenticationService.logOut();
    checkAuth();
    $location.path('/');
  };

  checkAuth();

});

