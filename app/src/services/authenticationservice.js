app.service('AuthenticationService', function (FirebaseService, $firebaseAuth) {
  var _auth = $firebaseAuth(FirebaseService.ref);

  return {
    isLoggedIn: function () {
      var auth = this.getAuth();
      return (auth != null);
    },

    getAuth: function () {
		  return _auth.$getAuth();
    },

    authorize: function (provider) {      
    	return _auth.$authWithOAuthPopup(provider || "google");
    },

    logOut: function () {
    	_auth.$unauth();
    }
  };

})


