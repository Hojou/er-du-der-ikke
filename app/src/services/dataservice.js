app.service('DataService', function ($firebaseArray, $filter, FirebaseService, $q) {
  var _fb = FirebaseService.ref;
  var date = new Date();
  date.setHours(0,0,0,0);

  var _employees = $firebaseArray(_fb.child("employees"));
  var _dates = $firebaseArray(_fb.child("dates").orderByChild("date").startAt(date.valueOf()).limitToFirst(10));

  function cleanDate(date) {
    console.log('employee and date', _employees, date);
    var index = date.employees.length;
    while (index--) {
      var employeeName = date.employees[index];
      var found = !employeeName || $filter('filter')(_employees, {name : employeeName}).length > 0;
      if (!found) {
        date.employees.splice(index, 1);
      }
    }
  }

  function _triggerNgInitReRunHack(key) {
    var record = _dates.$getRecord(key)
    if (!record)
      return;

    cleanDate(record);
    var index = _dates.indexOf(record);
    _dates[index] = angular.copy(record);
  }

  _dates.$watch(function (eventData) {
    _triggerNgInitReRunHack(eventData.key);
  });

  $q.all([_dates.$loaded(), _employees.$loaded()]).then(function() {
    console.log('cleanAllDates', _dates, _employees);
    angular.forEach(_dates, function (date) {
      cleanDate(date);
    });
  });

  return {
    getEmployees: function () {
    	return _employees;
    }, 

    getDates: function () {
    	return _dates;
    },

    createDate: function (date) {
  		var formattedDate = $filter('date')(date,'dd-MM');
  		_dates.$add({ txt : formattedDate, date : date.valueOf(), employees : [""] });
    },

    removeDate: function (date) {
    	_dates.$remove(date);
    },

    createEmployee: function (person) {
    	var employee = angular.copy(person);
    	_employees.$add(employee);
    },

    removeEmployee: function (employee) {
    	_employees.$remove(employee);
    	//TODO: Remove relation to dates? 
    },

    updateEmployee: function (employee) {
    	_employees.$save(employee);
    },

    assignEmployee: function (employee, authId) {
      angular.forEach(_employees, function (e) {
        if (e.assigned == authId) {
          delete e.assigned;
          _employees.$save(e);
        }
      });

      if (!employee.assigned) {
        employee.assigned = authId;
        _employees.$save(employee);
      }
    },

    getState: function (employee, date) {
    	return { state : date.employees.indexOf(employee.name) < 0 };
    },

    saveState: function(employee, date, state) {
  		var index = date.employees.indexOf(employee.name);

  		if (state && index >= 0) {
  			date.employees.splice(index, 1);
  		} else if (!state && index < 0) {
  			date.employees.push(employee.name);
  		} else {
  			return;
  		}

  		_dates.$save(date);
    }

  }  
})


